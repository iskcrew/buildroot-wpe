#!/bin/sh

mkdir -p output/host/usr/bin
mkdir -p output/host/bin
ln -s /usr/bin/python2 output/host/usr/bin/python2
ln -s output/usr/bin/c_rehash output/bin/c_rehash

make rpi_isk_defconfig
make
